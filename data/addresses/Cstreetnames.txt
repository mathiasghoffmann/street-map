C.A. Bluhmesvej
Cæciliavej
Cæcilieparken
C.A. Hansens Vej
Callesensgade
Callesensvej
Callisensvej
Callunavej
Calvinsvej
Camilla Larsens Vej
Camilla Nielsens Vej
Camillevej
Camillo Bruuns Vej
Camma Larsen-Ledets Vej
Campanella
Campingpladsvej
Campingvej
Campus Alle
Campusvej
Canada
Canadavej
C. Andersens Passage
Candidusvej
C.A. Nielsensvej
Cannerslundvej
C.A. Olesens Gade
C.A. Olesens Vej
Capellas Alle
Capellavænget
Capellavej
Caprifolievej
Caprifolvej
Caprivej
Capt Rottbølls Vej
Cargo Centervej
Cargovej
Carinaparken
Carinavænget
Carinavej
Carinvej
Caritasvej
Carit Etlars Alle
Carit Etlars Gade
Carit Etlarsvej
Carit Etlars Vej
Carl Albretsens Vej
Carl Alstrupsvej
Carl Amossensvej
Carl Baggers Alle
Carl Baggers Allé
Carl Bernhards Vej
Carl Bertelsens Gade
Carl Blochs Alle
Carl Blochs Gade
Carl Blochs Vej
Carl Bødker Nielsens Vej
Carl Bremers Vej
Carl Brissonsvej
Carl Bruuns Vej
Carl Collatz Vej
Carl Danfeldts Alle
Carl Drechslers Vej
Carl E. Lentz Vej
Carl Ewaldsvej
Carl Ewalds Vej
Carl Feilbergs Vej
Carl Fischersvej
Carl Günthers Vej
Carl Gustavs Gade
Carl Gustavs Vej
Carl Hansens Allé
Carl Hansens Vej
Carl Holst-Knudsens Vej
Carl Jacobsens Vej
Carl Jensens Vej
Carl Johansens Vej
Carl Johans Gade
Carl J. Ulrichs Vænge
Carl Klitgaards Vej
Carl Klitgårds Vej
Carl Krebs' Vej
Carl Langes Vej
Carl Lendorfs Vej
Carl Lindams Vej
Carl Lunds Vænge
Carl Lunds Vej
Carl Magnussensvej
Carl Medingsvej
Carl Moes Vej
Carl Møllers Alle
Carl Nielsens Allé
Carl Nielsens Plads
Carl Nielsensvej
Carl Nielsens Vej
Carlo Wognsens Vej
Carl Petersens Alle
Carl Petersensgade
Carl Pipers Vej
Carl Plougs Vænge
Carl Plougs Vej
Carl Rasmussensvej
Carl Reffsvej
Carl Roses Vej
Carl Rothes Vej
Carlsbergvej
Carlsen-Skiødts Vej
Carlsensvej
Carlsgade
Carlsgavevej
Carlshøjvej
Carlsmindeparken
Carlsmindevej
Carl Ståltrådsvej
Carlsvej
Carl Svenstrups Vej
Carlsvognsvej
Carl Th. Dreyers Vej
Carl Thomsensvej
Carl Th. Zahles Gade
Carl Torps Vej
Carl Von Rosensvej
Carl Zahlmannsvej
Carl Zeiss Vej
Carøesgade
Caroline Amalievej
Caroline Amalie Vej
Carolinelund
Carolinelundsvej
Caroline Mathilde Sti
Caroline Mathildes Vej
Caroline Mathilde Vej
Carolinevej
Carsten Hauchs Vænge
Carsten Hauchs Vej
Carsten Niebuhrs Gade
Carstensens Alle
Carstensgade
Carstensvej
Carstenvej
Casa Sanavej
Casinotorvet
Caspar Brands Plads
Caspar Müllers Gade
Casper Møllers Vej
Caspersvej
Cassiopeiavej
Castbergsvej
Castenschioldsgade
Castenschioldsvej
Castenskjoldsvej
Castor Alle
Castorvænget
Castorvej
Caswellsvej
Catharinasvej
Catherine Boothsvej
Cathrinebergvej
Cathrinebjergvej
Cathrinestræde
Cathrinevej
C.A. Thyregods Vej
Catolhavegyden
Catrinevej
Cavilas Gård
C.C. Halls Vej
C.C. Iversens Vej
C. D. Schrødersvej
C.E. Carlsens Vej
C. E. Christiansens Vej
Ceciliavej
Cecilieborgvej
Cecilie Marie Vej
Ceciliesminde
Cecilievej
Cederfeldsgade
Cederfeldsvej
Cedergangen
Cederkrattet
Cederlunden
Cederstien
Cedervænget
Cedervangen
Cedervej
C.E. Fengers Vej
Cellovej
Cellulosevej
Cementstøbervej
Cementvej
Cementvejen
Center Alle
Centerbakken
Center Boulevard
Centerhavn
Centerholmen
Centerparken
Centerpassagen
Centerpladsen
Centertorvet
Centervænget
Centervej
Centervejen
Centervej Syd
Center Vest
Centralgården
Centralgårdsvej
Centralskolevej
Centralvej
Centret
Centrifugevej
Centrum
Centrumgaden
Centrumgården
Centrumpladsen
Centrumsgaden
Ceres Alle
Ceresbyen
Ceresvænget
Ceresvej
Cerutten
Ceylonvej
C.F. Aagaards Vej
C.F. Holbechs Vej
C.F. Jessens Vej
C.F. Møllers Alle
C. F. Møllers Allé
C.F. Møllers Allé
C.F. Richs Vej
C.F. Tietgens Boulevard
C. F. Tietgens Vej
C.F. Tietgens Vej
Chamottevej
C. Hansensvej
Charles Hansens Vej
Charles Lindberghs Vej
Charles Roos Vænget
Charlesvej
Charlotteager
Charlotte Amalies Kvarter
Charlotte Amalievej
Charlotte Ammundsens Plads
Charlottedal Alle
Charlottedalsvej
Charlottegårdsvej
Charlottehøj
Charlottehøj, Haveforening
Charlottehøjsvej
Charlotte Muncks Vej
Charlottenlund Stationsplads
Charlottenlundvej
Charlotteparken
Charlottesvej
Charlottes Vej
Charlottevej
Cherryvej
Chilikæret
Chopinsvej
Chorsvej
Chr. Bergs Vej
Chr. Bruuns Vej
Chr. Damsgaards Gade
Chr. d. IXs Vej
Chr. E. Bartholdys Alle
Chresten Jacobsens Vej
Chresten Krommes Vej
Chrestensmindevej
Chr. Hansens Vej
Christen Bergs Allé
Christen Bergs Vej
Christen Købkes Gade
Christen Kolds Alle
Christen Koldsvænget
Christen Koldsvej
Christen Kolds Vej
Christian 10 Gade
Christian 2.s Vej
Christian 3 Vej
Christian 4 Vej
Christian 8.s Vej
Christian Adams Vej
Christian Ågårdsvej
Christian Andersens Vej
Christian Bachsvej
Christian Christensensvej
Christian Dalgas Vej
Christian Damsgaards Gade
Christian Dannings Vej
Christian E. Bartholdys Alle
Christianehøj
Christian Eilers Vej
Christian Erichsøns Vej
Christian Eriksens Vej
Christian Fabers Vej
Christian F. Becks Vej
Christian Filtenborgs Plads
Christian Fischers Gade
Christian F. Jensensvej
Christian Gades Vej
Christian Greens Alle
Christian Hansensvej
Christian Hansens Vej
Christian Hauchs Alle
Christian Hauns Alle
Christian Hostrups Vej
Christian Husteds Vej
Christian II's Allé
Christian II's Gade
Christian IVs Vej
Christian IX's Gade
Christian IX's Vej
Christian Jacobsens Vej
Christian Jensens Vej
Christian Jørgensensvej
Christian Kellers Vej
Christian Kiers Plads
Christian Kiilsgaards Vej
Christian Knudsens Vej
Christian Kusk Vej
Christian Lindes Vej
Christian Ludwigs Vej
Christian L. Westergårds Alle
Christian Mathiesens Gade
Christian Molbechs Vej
Christian Mølsteds Gade
Christian M. Østergaards Vej
Christian Nielsensvej
Christian Nielsens Vej
Christian Ohlsen Vænget
Christian Paulsens Vej
Christian Pedersvej
Christian Pelles Vej
Christian Petersensvej
Christian Petersens Vej
Christian Rasmussens Vej
Christian Richardts Vej
Christiansberg
Christiansborggade
Christiansborg Ridebane
Christiansborg Slot
Christiansborg Slotsplads
Christiansborgvej
Christians Brygge
Christian Schmidts Vej
Christiansdal
Christiansdalsvej
Christiansdalvej
Christiansfeld Landevej
Christiansfeldvej
Christiansgade
Christiansgave
Christianshåbsvej
Christianshave
Christianshavns Kanal
Christianshavns Torv
Christianshavns Voldgade
Christianshedevej
Christianshøj
Christianshøjvej
Christiansholmsgade
Christiansholm Slot
Christiansholms Mose
Christiansholms Parallelvej
Christiansholms Parkvej
Christiansholms Tværvej
Christiansholmsvej
Christianshusvej
Christianshvilevej
Christianslund
Christianslundsvej
Christianslundvej
Christianslyst
Christiansminde
Christiansminde, Havekoloni
Christiansmindeparken
Christiansmindevej
Christian Sørensens Vej
Christiansøvænget
Christiansøvej
Christians Plads
Christiansrovej
Christian Stampes Vej
Christians Torv
Christiansvænget
Christiansvej
Christian Svendsens Gade
Christian Svendsens Vej
Christian Thisteds Vej
Christian VII's Alle
Christian VII's Vej
Christian Wærums Gade
Christian Winthers Vej
Christian X's Alle
Christian X's Vej
Christian Ydes Vej
Christinavej
Christinedahlsvej
Christinedalsvej
Christine Frederikke Vej
Christinehøjvej
Christinelundsvej
Christinelystvej
Christmas Møllers Plads
Christmas Møllers Vej
Christoffer 2 Vej
Christoffers Alle
Christofferslundvej
Christoffersvej
Christopher Boecks Alle
Chr. Jensens Vej
Chr. Jydesvej
Chr Kongsbaks Vej
Chr. Kornmaalers Vej
Chr. Larsens Vej
Chr. Lehns Vænge
Chr. Limkildes Vej
Chr. Lunds Allé
Chr. Lundsgaards Vej
Chr. Lynges Vej
Chr. Madsensvej
Chr. Molbechs Vej
Chr. Møllers Vej
Chr. Nielsens Plads
Chr Nielsens Vej
Chr. Palms Vej
Chr Pedersensvej
Chr. Pedersensvej
Chr Rasmussens Vej
Chr. Rhuusvej
Chr. Richardts Vej
Chr. Richtersgade
Chr. Schous Vej
Chr. Schrøders Gade
Chr. Sonnes Vej
Chrst Boecksvej
Chr V Jensensvej
Chr Winthers Vej
Chr. Winthers Vej
Chr X's Vej
Chr. X''s Vej
Chr. Xs Vej
Chr. X Vej
C.H. Ryders Vej
Churchillparken
Cikorievænget
Cikorievej
Cimbrervej
Cirkelhusene
Cirklen
Cirklen A
Cirklen B
Cirklen C
Cirklen D
Cirklen E
Cirklen F
Cirklen G
Citadelvej
Citronlunden
Citronvej
City 2
City Centret
Civagårdsvej
C.J. Brandts Vej
C.J. Brostrøms Vej
C.J. Frandsens Vej
C.J. Thaningsvej
Clacksvej
Claessensvej
Clara Frijsvej
Clara Pontoppidans Vej
Clarasvej
Claravej
Clasonsborgvej
Classensgade
Classensvej
Claudisvej
Clausagervej
Claus Bergs Gade
Claus Cortsens Gade
Clausens Allé
Clausensvej
Claus Henrisvej
Clausholms Alle
Clausholmvej
Clauskær
Claus Norbysvej
Claus Petersens Alle
Clematisstien
Clematisvænget
Clematisvej
Clemens
Clemensgade
Clemensmindevej
Clemensvej
Clementsvej
Clemmenshave
Clemmessti
Clemmi Falk Vej
Clermontgade
C.L. Ibsens Vej
C.M. Larsens Alle
C. M. Madsen Vænget
C.M. Nielsens Vej
C.M. Rasmussens Vej
C.N. Petersens Vej
Coastergade
Coasterkaj
Coastervej
Cobavej
Codansvej
Codanvej
Cohrsvej
C.O. Jensens Vej
Colbjørnsensgade
Colbjørnsensvej
Coldingvej
Coldsvej
Colibristien
Colibrivej
Collinsgade
Colombovej
Columbiavej
Columbinevej
Columbusvej
Concordiavej
Conferencevej
Conradineslyst
Conrad Nyegårdsvej
Conradsminde
Conrads Vej
Constancevej
Constantiaparken
Constantiavej
Constantin Hansens Gade
Const Hansens Vej
Containerkajen
Containervej
Contortavej
Corasvej
Cordozasvinget
Corfitzvej
Coriolisvej
Corkgade
Corneliusmindevej
Coronavej
Cort Adelers Gade
Cort Adelersvej
Cort Adelers Vej
Cortesgyde
Cortesvej
Cortex Park
Cortinavej
Cortlandvænget
Cortlandvej
Cosmosvej
Cottageparken
Cottagevej
Cottaslow
Cox Orangevej
Coxvej
C. P. Holbølls Plads
C.P. Jensens Vej
C.P. Madsensvej
C.Q. Lunøes Vej
C. R. Brix Vej
Crilles Tønnesens Alle
Crinolinevænget
Cristinavej
Crocusvej
Cronborgvej
Crownvej
C. S. Møllers Vej
C. Steinckesvej
C. Syrach Larsens Vej
C.T. Barfoeds Vej
C.Th. Zahles Vej
Cubavej
Cumberlandsgade
Cumulusvej
Curdtslund
C-Vej
C.V.E. Knuths Vej
C.V. Jessens Gade
C.W. Obels Plads
Cylindervej
Cypernsvej
Cypernvej
Cypres Alle
Cypreshaven
Cypreslunden
Cypressen
Cyprestræet
Cypresvænget
Cypresvej
Czarensvej
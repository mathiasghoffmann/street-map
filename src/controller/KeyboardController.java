package controller;

import model.Model;
import view.CanvasView;
import view.MainWindowView;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Controls the actions taken, when certain keys are released.
 */
public class KeyboardController extends KeyAdapter {
    private Model model;
    private MainWindowView view;
    private CanvasView canvas;

    /**
     * Constructs a new KeyBoardController and add keylistener to the main window as well as the canvas.
     * @param v the main window
     * @param c the canvas
     * @param m the model
     */
    public KeyboardController(MainWindowView v, CanvasView c, Model m) {
        view = v;
        canvas = c;
        model = m;
        view.window.addKeyListener(this);
        canvas.addKeyListener(this);
    }

    /**
     * Takes a specific action, when a specific key is released.
     * @param e the keyevent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyChar()) {
            case 'x':
                canvas.toggleAntiAliasing();
                break;
            case '+':
                canvas.zoomToCenter(1.1);
                break;
            case '-':
                canvas.zoomToCenter(1/1.1);
                break;
            default:
                break;
        }

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                canvas.pan(0, 10);
                break;
            case KeyEvent.VK_LEFT:
                canvas.pan(10, 0);
                break;
            case KeyEvent.VK_DOWN:
                canvas.pan(0, -10);
                break;
            case KeyEvent.VK_RIGHT:
                canvas.pan(-10, 0);
                break;
            default:
                break;
        }
    }
}

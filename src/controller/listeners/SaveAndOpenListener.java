package controller.listeners;

import model.Model;
import view.LoadingPopUp;
import view.MainWindowView;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Listener for the save and load button in the menu in the menu bar.
 */
public class SaveAndOpenListener implements ActionListener {
    boolean isSave;
    final JFileChooser fc = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("bin files, osm files and zip directories", "bin", "osm", "zip");
    Model model;
    private LoadingPopUp loadingPopup;

    /**
     * Constructs a new SaveAndOpenListener for either saving or loading of the model
     * @param isSave whether the program is going to save or load
     * @param model the model
     */
    public SaveAndOpenListener(boolean isSave, Model model) {
        this.isSave = isSave;
        this.model = model;
        fc.setFileFilter(filter);
    }

    /**
     * Saves or loads the current model to binary, when the corresponding button is pressed.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Runnable loading = new Runnable() {
            @Override
            public void run() {
                loadingPopup = new LoadingPopUp();
            }
        };
        int returnVal;
        if (isSave) {
            returnVal = fc.showSaveDialog(MainWindowView.window);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                model.save(fc.getSelectedFile().getPath() + ".bin");
            }
        } else {
            returnVal = fc.showOpenDialog(MainWindowView.window);
            if (returnVal == JFileChooser.APPROVE_OPTION){
                Thread thread = new Thread() {
                    public void run() {
                        try {
                            SwingUtilities.invokeAndWait(loading);
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                        }
                        model.load(fc.getSelectedFile().getPath());
                        loadingPopup.getFrame().dispose();
                    }
                };
                thread.start();
            }
        }
    }
}

package controller.listeners;

import model.AddressFinder;
import model.Model;
import view.CanvasView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SearchListener implements ActionListener {
    private JComboBox searchComboBox;
    private AddressFinder af;

    public SearchListener(JComboBox searchComboBox, Model m, CanvasView c) {
        this.searchComboBox = searchComboBox;
        this.af = new AddressFinder(m, c);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        af.panToAddress(searchComboBox.getEditor().getItem().toString());
    }
}

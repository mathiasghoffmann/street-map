package helpers;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Controls the suggestions in search fields.
 * Is called whenever a search field is meant to auto suggest an address.
 */
public class AutoSuggestor {
    private JComboBox comboBox;
    private ArrayList<String> suggestions;

    public AutoSuggestor(JComboBox comboBox, ArrayList<String> suggestions) {
        this.suggestions = suggestions;
        this.comboBox = comboBox;
        this.comboBox.setLightWeightPopupEnabled(false);

        updateSuggestions(suggestions);
    }

    public void updateSuggestions(ArrayList<String> suggestions) {
        if (!suggestions.isEmpty()) {
            comboBox.hidePopup();
            String item = comboBox.getEditor().getItem().toString();
            comboBox.removeAllItems();
            comboBox.addItem(item);
            for (String suggestion : suggestions)
                comboBox.addItem(suggestion);
        }
    }
}


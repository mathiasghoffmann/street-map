package helpers;

import java.awt.geom.Point2D;
import java.io.Serializable;
/**
 * A class to keep information about a point of interest.
 */
public class PointOfInterest implements Serializable {
    private Point2D p;
    private String text;
    /**
     * Constructs a new point of interest.
     * @param p the point of interests model coordinates.
     * @param text The name / description of the point of interest.
     */
    public PointOfInterest(Point2D p, String text) {
        this.p = p;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Point2D getPoint() {
        return p;
    }
}
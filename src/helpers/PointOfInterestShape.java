package helpers;

import java.awt.*;
/**
 * A class to remember the shape of a drawn point of interest.
 */
public class PointOfInterestShape {
    Shape shape;
    int indexinpointsofinterest;
    /**
     * Constructs a new shape, for a point of interest.
     * @param shape the shape of the point of interest.
     * @param indexinpointsofinterest is the point of interests index in,
     *                               ArrayList<pointOfInterest> pointsOfInterest, in Model.
     */
    public PointOfInterestShape(Shape shape, int indexinpointsofinterest) {
        this.shape = shape;
        this.indexinpointsofinterest = indexinpointsofinterest;
    }

    public int getIndexInPointsOfInterest() {
        return indexinpointsofinterest;
    }

    public Shape getShape() {
        return shape;
    }
}
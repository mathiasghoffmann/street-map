package model;

import helpers.AddressMap;
import osm.OSMAddress;
import view.CanvasView;

import javax.swing.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/**
 * Used for finding addresses on the map.
 * Mainly called from the different listeners.
 *
 * @author Emil Kastholm Fischer
 */
public class AddressFinder {
    Model model;
    CanvasView canvas;
    AddressMap addresses;

    public AddressFinder (Model m, CanvasView c) {
        this.model = m;
        this.canvas = c;
        this.addresses = m.getAddresses();
    }

    public ArrayList<OSMAddress> findListOfAddresses(String input) {
        return addresses.get(input);
    }

    /**
     * Finds, at max, four addresses that starts with the given input in the given list of addresses.
     *
     * @param input         name of the address that needs to be found
     * @param inputList     list of addresses that is being searched through
     * @return              a list of the found addresses
     */
    public ArrayList<String> findFourAddressNames(String input, ArrayList<OSMAddress> inputList) {
        ArrayList<String> result = new ArrayList<>();
        for (OSMAddress address: inputList) {
            if (address.address.toLowerCase().startsWith(input.toLowerCase())) {
                result.add(address.address);
                if (result.size() == 4)
                    break;
            }
        }
        return result;
    }

    public Point2D findAddressCoordinates(String name) {
        ArrayList<OSMAddress> list = findListOfAddresses(name);
        for (OSMAddress address: list){
            if (address.address.toLowerCase().startsWith(name.toLowerCase()))
                return new Point2D.Double(address.getX(), address.getY());
        }
        return null;
    }

    private void setMarker(Point2D p) {
        canvas.drawSearchPoint(p);
    }

    public void panToRoute(String name) {
        findAndPan(name);
    }

    public void panToAddress(String name) {
        setMarker(findAndPan(name));
    }

    /**
     * Finds an address on the map that starts with the given name and then pans to it on the map.
     *
     * @param name  name of the address
     * @return      returns the point of the found address
     */
    private Point2D findAndPan(String name) {
        Point2D p = findAddressCoordinates(name);
        if (p == null) {
            JOptionPane.showMessageDialog(null, "No address, with the name " + name + ", was found! :-(");
            return null;
        }
        Point2D p2 = canvas.toScreenCoords(p);
        canvas.pan(-p2.getX() + canvas.getWidth()/2, -p2.getY() + canvas.getHeight()/2);
        while (canvas.getCurrentZoomLVL() > 0.5) {
            canvas.zoomToCenter(1.1);
        }
        return p;
    }
}

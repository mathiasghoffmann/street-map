package model;

import osm.OSMRoad;
import osm.OSMWayType;
import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A weighted graph.
 * Mainly used for route planning.
 * But also used in showing the nearest road in the toolbar at the bottom of the user interface.
 */
public class WeightedGraph implements Serializable {
    private KDTree tree;
    private ArrayList<Vertex> vertices;
    private Edge errorEdge;
    private HashMap<Long, Vertex> idToVertex = new HashMap<>(25);
    // index is used in one of the getClosestShape methods
    private int index;

    /**
     * Constructor for weighted graph using list of roads that has information for edges.
     * @param roads a list of osm roads from the model
     */
    public WeightedGraph(ArrayList<OSMRoad> roads) {
        vertices = new ArrayList<>();
        errorEdge = new Edge("Nothing Found");
        ArrayList<Shape> edges = new ArrayList<>();
        for (OSMRoad road : roads) {
            edges.add(new Edge(road.from, road.to, road.type, road.weight, road.isOneWay, road.name, road.startNodeId,
                    road.endNodeId, road.isDrivingAllowed, road.isCyclingAllowed, road.isWalkingAllowed, road.maxSpeed,
                    road.startNodeTrafficSignal, road.endNodeTrafficSignal));
        }
        tree = new KDTree(edges, 200);
        idToVertex = null;
    }

    /**
     * Gets the closest <code>Edge</code> to a point.
     * @param x the x coordinate to the point
     * @param y the y coordinate to the point
     * @return the closest edge
     */
    public Edge getClosestEdge(double x, double y) {
        ArrayList<Shape> edges = tree.getClosestShapes(x, y);
        if (edges.isEmpty())
            return errorEdge;
        Point2D position = new Point2D.Double(x, y);
        Edge closestEdge = (Edge) edges.get(0);
        double length = closestEdge.ptSegDistSq(position);
        for (int i = 1; i < edges.size(); i++) {
            Edge e = (Edge) edges.get(i);
            double tmp = e.ptSegDistSq(position);
            if (tmp < length) {
                length = tmp;
                closestEdge = e;
            }
        }
        return closestEdge;
    }

    /**
     * Gets the closest <code>Edge</code> to a point, where the specific vehicle is allowed.
     * @param x the x coordinate to the point
     * @param y the y coordinate to the point
     * @param vehicle the specific vehicle used, has to be either "Car", "Bike" or "Walk"
     * @return the closest edge
     */
    public Edge getClosestEdge(double x, double y, String vehicle) {
        ArrayList<Shape> edges = tree.getClosestShapes(x, y);
        if (edges.isEmpty())
            return errorEdge;
        Point2D position = new Point2D.Double(x, y);

        index = 0;
        Edge closestEdge = getAllowedEdge(vehicle, edges);
        double length = closestEdge.ptSegDistSq(position);
        while(index < edges.size()) {
            Edge e = getAllowedEdge(vehicle, edges);
            double tmp = e.ptSegDistSq(position);
            if (tmp < length) {
                length = tmp;
                closestEdge = e;
                index++;
            }
            else index++;
        }
        return closestEdge;
    }

    /**
     * Gets an allowed edge from <code>edges</code> for the specific vehicle.
     * @param vehicle the specific vehicle used, has to be either "Car", "Bike" or "Walk"
     * @param edges the list of closest edges
     * @return the allowed edge
     */
    private Edge getAllowedEdge(String vehicle, ArrayList<Shape> edges) {
        // length is the distance from the closestEdge to the position
        Edge edge = null;
        boolean allowed = false;
        while(!allowed && index < edges.size()) {
            edge = (Edge) edges.get(index);
            switch (vehicle) {
                case "Car":
                    if (edge.isDrivingAllowed) allowed = true;
                    else index++;
                    break;
                case "Bike":
                    if (edge.isCyclingAllowed) allowed = true;
                    else index++;
                    break;
                case "Walk":
                    if (edge.isWalkingAllowed) allowed = true;
                    else index++;
                    break;
            }
        }
        return edge;
    }

    /**
     * Gets the closest vertex to the <code>position</code>.
     * When finding the closestEdge in this method the choice of vehicle is taken into account.
     * @param position the coordinates to use in the search for the vertex
     * @param vehicle the specific vehicle used, has to be either "Car", "Bike" or "Walk"
     * @return the closest vertex
     */
    public Vertex getClosestVertex(Point2D position, String vehicle) {
        Edge e = getClosestEdge(position.getX(), position.getY(), vehicle);
        if (position.distance(e.getFrom()) < position.distance(e.getTo()))
            return e.startVertex;
        return e.endVertex;
    }

    public String getNameOfClosestEdge(double x, double y) {
        return getClosestEdge(x, y).name;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    /**
     * Represents a vertex in the weighted graph.
     * A vertex is also list of edges. Each vertex contains the list of edges that extends from that vertex.
     */
    public class Vertex extends ArrayList<Edge> implements Comparable<Vertex> {
        long id;
        /**
         * The distance from the vertex to the source vertex on the shortest path.
         */
        Double distTo;
        /**
         * The last edge on the shortest path from the source vertex to the vertex.
         */
        Edge edgeTo;
        /**
         * The estimated distance from the vertex to the destination vertex in a straight line.
         */
        Double estimationToDestination;
        /**
         * Whether we have visited the node before, when calculating the route.
         */
        Boolean visited;
        /**
         * The coordinates for the vertex.
         */
        Point2D coordinate;
        // trafficSignal is not really used
        boolean trafficSignal;

        /**
         * Constructor for the vertex.
         * @param id the id for the vertex
         * @param coordinate the coordinate for the vertex
         * @param trafficSignal whether the vertex is a traffic signal
         */
        Vertex(long id, Point2D coordinate, boolean trafficSignal) {
            this.id = id;
            this.coordinate = coordinate;
            this.trafficSignal = trafficSignal;
        }

        @Override
        public int compareTo(Vertex v) {
            return Double.compare(this.distTo + this.estimationToDestination, v.distTo + v.estimationToDestination);
        }

        public Edge getEdgeTo() {
            return edgeTo;
        }

        public Point2D getCoordinate() {
            return coordinate;
        }
    }

    /**
     * Represents an edge in the weighted graph.
     * Basically represents a small piece of road.
     */
    public class Edge extends Line2D.Double {
        OSMWayType type;
        double weight;
        String name;
        Vertex startVertex;
        Vertex endVertex;
        boolean isOneWay;
        boolean isDrivingAllowed;
        boolean isCyclingAllowed;
        boolean isWalkingAllowed;
        double maxSpeed;
        double timeToTraverse;
        /**
         * The bearing of the edge, which is a value in the range [0-360[, where 0 represents north, 90 east and so on.
         */
        double direction;

        private Edge(Point2D from, Point2D to, OSMWayType type, double weight, boolean isOneWay, String name,
                    long startVertexId, long endVertexId, boolean isDrivingAllowed, boolean isCyclingAllowed,
                    boolean isWalkingAllowed, double maxSpeed, boolean startNodeTrafficSignal, boolean
                            endNodeTrafficSignal) {
            super(from, to);
            this.type = type;
            this.weight = weight;
            this.isOneWay = isOneWay;
            this.name = name;
            this.isDrivingAllowed = isDrivingAllowed;
            this.isCyclingAllowed = isCyclingAllowed;
            this.isWalkingAllowed = isWalkingAllowed;
            this.maxSpeed = maxSpeed;
            this.timeToTraverse = weight / maxSpeed;

            Vertex startVertex = addOrGetVertex(startVertexId, from, startNodeTrafficSignal);
            this.startVertex = startVertex;
            if (startVertex != null) {
                startVertex.add(this);
            }

            Vertex endVertex = addOrGetVertex(endVertexId, to, endNodeTrafficSignal);
            this.endVertex = endVertex;
            if (endVertex != null) {
                endVertex.add(this);
            }
        }

        private Edge (String name) {
            this.name = name;
        }

        public Point2D getFrom(){
            return getP1();
        }

        public Point2D getTo() {
            return getP2();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getWeight() {
            return weight;
        }

        public Vertex getStartVertex() {
            return startVertex;
        }

        public Vertex getEndVertex() {
            return endVertex;
        }

        public double getDirection() {
            return direction;
        }

        public void setDirection(double direction) {
            this.direction = direction;
        }

        private Vertex addOrGetVertex(long id, Point2D coordinate, boolean trafficSignal) {
            Vertex v = idToVertex.get(id);
            if (v == null) {
                v = new Vertex(id, coordinate, trafficSignal);
                vertices.add(v);
                idToVertex.put(id, v);
            }
            return v;
        }
    }
}

package osm;

import java.io.Serializable;

/**
 * A class to hold information about an Adress.
 */
public class OSMAddress implements Serializable {

    public String address;
    private double x;
    private double y;

    /**
     * Constructs an OSMAddress object.
     * @param address The name of the address.
     * @param node A point with the coordinates of the address (model coordinates)
     */
    public OSMAddress(String address, OSMNode node) {
        this.address = address;
        this.x = node.getLon();
        this.y = node.getLat();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}

package osm;

/**
 *  A class to hold information about a node.
 */
public class OSMNode {
    private double lon, lat;
    public boolean traffic_signals;

    /**
     * Constructs an OSMNode
     * @param lon   model x coordinate
     * @param lat   model y coordinate
     */
    public OSMNode(double lon, double lat) {
        this.lon = lon;
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }
}

package osm;
/**
 * A class to hold information about a place with a name (cities and islands).
 */

import java.awt.geom.*;

public class OSMPlaceName extends Line2D.Double {
    private String placeName;

    public OSMPlaceName(Point2D p, String placeName) {
        super(p,p);
        this.placeName = placeName;
    }

    public String getPlaceName() {
        return placeName;
    }
}

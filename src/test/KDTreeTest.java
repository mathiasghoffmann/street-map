package test;

import model.KDTree;
import org.junit.Before;
import org.junit.Test;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import static org.junit.Assert.*;

public class KDTreeTest {

    //We create 31 Rectangle2D objects to have a wide range, of objects to test.
    Rectangle2D rect = new Rectangle2D.Double(0, 0, 200, 200);
    Rectangle2D rect2 = new Rectangle2D.Double(304, 25, 160, 190);
    Rectangle2D rect3 = new Rectangle2D.Double(500, 300, 20, 180);
    Rectangle2D rect4 = new Rectangle2D.Double(100, 900, 200, 170);
    Rectangle2D rect5 = new Rectangle2D.Double(1000, 14, 250, 160);
    Rectangle2D rect6 = new Rectangle2D.Double(714, 546, 200, 150);
    Rectangle2D rect7 = new Rectangle2D.Double(760, 405, 25, 140);
    Rectangle2D rect8 = new Rectangle2D.Double(0, 1, 100, 130);
    Rectangle2D rect9 = new Rectangle2D.Double(0, 0, 400, 500);
    Rectangle2D rect10 = new Rectangle2D.Double(100, 100, 100, 100);
    Rectangle2D rect11 = new Rectangle2D.Double(200, 200, 200, 200);
    Rectangle2D rect12 = new Rectangle2D.Double(300, 300, 300, 300);
    Rectangle2D rect13 = new Rectangle2D.Double(400, 400, 400, 400);
    Rectangle2D rect14 = new Rectangle2D.Double(500, 500, 500, 500);
    Rectangle2D rect15 = new Rectangle2D.Double(600, 600, 600, 600);
    Rectangle2D rect16 = new Rectangle2D.Double(700, 700, 700, 700);
    Rectangle2D rect17 = new Rectangle2D.Double(800, 800, 800, 800);
    Rectangle2D rect18 = new Rectangle2D.Double(900, 900, 900, 900);
    Rectangle2D rect19 = new Rectangle2D.Double(11, 11, 11, 11);
    Rectangle2D rect20 = new Rectangle2D.Double(25, 25, 25, 25);
    Rectangle2D rect21 = new Rectangle2D.Double(56, 56, 56, 56);
    Rectangle2D rect22 = new Rectangle2D.Double(102, 102, 102, 102);
    Rectangle2D rect23 = new Rectangle2D.Double(501, 501, 501, 501);
    Rectangle2D rect24 = new Rectangle2D.Double(500, 500, 300, 121);
    Rectangle2D rect25 = new Rectangle2D.Double(123, 123, 123, 123);
    Rectangle2D rect26 = new Rectangle2D.Double(234, 234, 234, 234);
    Rectangle2D rect27 = new Rectangle2D.Double(345, 345, 345, 345);
    Rectangle2D rect28 = new Rectangle2D.Double(645, 654, 644, 322);
    Rectangle2D rect29 = new Rectangle2D.Double(101, 230, 2, 21);
    Rectangle2D rect30 = new Rectangle2D.Double(9, 1, 2, 1000);
    Rectangle2D rect31 = new Rectangle2D.Double(654, 675, 21, 90);

    KDTree.Node node;
    private ArrayList<Shape> shapes;
    private KDTree kdt;

    Rectangle2D viewRect = new Rectangle2D.Double(0, 0, 200, 200);

    @Before
    /**
     * This method is called before every testmethod is tried.
     * The KDTree which we test, is created here.
     */
    public void setUp() throws Exception {

        int leafsize = 1;
        shapes = new ArrayList<>();

        shapes.add(rect);
        shapes.add(rect2);
        shapes.add(rect3);
        shapes.add(rect4);
        shapes.add(rect5);
        shapes.add(rect6);
        shapes.add(rect7);
        shapes.add(rect8);
        shapes.add(rect9);
        shapes.add(rect10);
        shapes.add(rect11);
        shapes.add(rect12);
        shapes.add(rect13);
        shapes.add(rect14);
        shapes.add(rect15);
        shapes.add(rect16);
        shapes.add(rect17);
        shapes.add(rect18);
        shapes.add(rect19);
        shapes.add(rect20);
        shapes.add(rect21);
        shapes.add(rect22);
        shapes.add(rect23);
        shapes.add(rect24);
        shapes.add(rect25);
        shapes.add(rect26);
        shapes.add(rect27);
        shapes.add(rect28);
        shapes.add(rect29);
        shapes.add(rect30);
        shapes.add(rect31);

        kdt = new KDTree(shapes, leafsize);

        KDTree.Node node = kdt.new Node(true);
    }

    @Test
    /**
     *
     *Here we test that that visibleShapes() method is working the way it should.
     *We test the method for every Rectangle2D object we have created.
     */
     public void visibleShapes() throws Exception {
        assertTrue(kdt.visibleShapes(viewRect).contains(rect));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect8));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect9));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect10));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect19));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect20));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect21));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect22));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect25));
        assertTrue(kdt.visibleShapes(viewRect).contains(rect30));
        if (kdt.visibleShapes(viewRect).contains(rect2)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect3)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect4)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect5)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect6)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect7)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect11)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect12)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect13)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect14)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect15)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect16)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect17)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect18)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect23)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect24)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect26)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect27)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect28)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect29)) {
            fail();
        }
        if (kdt.visibleShapes(viewRect).contains(rect31)) {
            fail();
        }
    }

    @Test
    /**
     * We test that the getClosestShapes() method works.
     * The method creates an ArrayList, and we assert what it should, and should'nt, contain, and test that.
     */
    public void getClosestShape() throws Exception {
        if (!kdt.getClosestShapes(200, 200).contains(rect22)) {
            fail();
        }
        if (!kdt.getClosestShapes(200, 200).contains(rect25)) {
            fail();
        }
        if (!kdt.getClosestShapes(200, 200).contains(rect4)) {
            fail();
        }
        if (!kdt.getClosestShapes(200, 200).contains(rect11)) {
            fail();
        }
        if (!kdt.getClosestShapes(200, 200).contains(rect9)) {
            fail();
        }
        if (kdt.getClosestShapes(200, 200).contains(rect10)) {
            fail();
        }
        if (kdt.getClosestShapes(200, 200).contains(rect20)) {
            fail();
        }
    }

}
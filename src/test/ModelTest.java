package test;

import model.Model;
import org.junit.Test;

public class ModelTest {
    private Model m;

    @Test
    /**Here we test that we can create a model based on a testmap, that we have created. Since the Model constructor has a
     * methodcall, to the load() method, and the load() method has a call to the readFromOSM() method,
     * we are testing that all of this works together, like it is supposed to.
    */
     public void testSomething() throws Exception{
        m = new Model("C:/Users/Oliver/IdeaProjects/BFST18_Group28/data/testmap/test_2.osm");
    }
    }

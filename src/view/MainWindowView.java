package view;

import model.Model;
import controller.listeners.SaveAndOpenListener;
import javax.swing.*;
import java.awt.*;

public class MainWindowView{
    public static JFrame window;

    /**
     * The main window of the program.
     * @param sb the search box
     * @param cv the canvas
     * @param sm the side menu
     * @param tb the toolbar
     * @param m the model
     */
    public MainWindowView(SearchBox sb, CanvasView cv, SideMenu sm, Toolbar tb, Model m) {
        window = new JFrame("Førsteårsprojekt");
        window.setFocusable(true);

        // Sets the top menu bar
        JMenuBar menuBar = new JMenuBar();
        window.setJMenuBar(menuBar);
        JMenu file = new JMenu("File");
        menuBar.add(file);
        JMenuItem save = new JMenuItem("Save");
        JMenuItem open = new JMenuItem("Open");
        JMenu settings = new JMenu("Color settings");
        JMenuItem setColorsToStandard = new JMenuItem("Standard colors");
        JMenuItem setColorsToNegative = new JMenuItem("Negative colors");
        JMenuItem setToGreyScale = new JMenuItem("Black and white");
        JMenuItem darkerColors = new JMenuItem("Darker colors");
        JMenuItem brighterColors = new JMenuItem("Brighter colors");
        JMenuItem toggleAntiAliasing = new JMenuItem("Toggle Antialiasing");
        JMenuItem togglePointsOfInterest = new JMenuItem("Toggle points of interest");

        file.add(save);
        file.add(open);
        settings.add(setColorsToStandard);
        settings.add(setColorsToNegative);
        settings.add(setToGreyScale);
        settings.add(darkerColors);
        settings.add(brighterColors);
        settings.add(toggleAntiAliasing);
        settings.add(togglePointsOfInterest);
        file.add(settings);
        save.addActionListener(new SaveAndOpenListener(true, m));
        open.addActionListener(new SaveAndOpenListener(false, m));
        setColorsToStandard.addActionListener(e -> {cv.setColorsToStandard();});
        setColorsToNegative.addActionListener(e -> {cv.setColorsToNegative();});
        setToGreyScale.addActionListener(e -> {cv.setToGreyScale();});
        darkerColors.addActionListener(e -> {cv.darkerColors();});
        brighterColors.addActionListener(e -> {cv.brighterColors();});
        toggleAntiAliasing.addActionListener(e -> {cv.toggleAntiAliasing();});
        togglePointsOfInterest.addActionListener(e -> {cv.togglePointsOfInterest();});

        // Opens window in full screen, when starting program
        window.setExtendedState(JFrame.MAXIMIZED_BOTH);

        window.setLayout(new BorderLayout());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Placing CanvasAndSearch panel
        JPanel canvasAndSearch = new JPanel();
        canvasAndSearch.setLayout(new BorderLayout());
        window.add(canvasAndSearch, BorderLayout.CENTER);

        // Placing SearchBox
        canvasAndSearch.add(sb, BorderLayout.NORTH);
        sb.setPreferredSize(new Dimension(1000, 37));

        // Placing Canvas
        canvasAndSearch.add(cv, BorderLayout.CENTER);

        // Placing Side Menu
        sm.setPreferredSize(new Dimension(500, 800));
        window.add(sm, BorderLayout.WEST);

        // Placing Toolbar
        tb.setPreferredSize(new Dimension(1500, 37));
        window.add(tb, BorderLayout.SOUTH);

        window.pack();
        window.setVisible(true);
        // put screen to correct place on canvas
        cv.pan(-m.getMinLon(), -m.getMaxLat());
        cv.zoom(window.getWidth() / (m.getMaxLon() - m.getMinLon()), 0, 0);
    }

}

package view;

import controller.listeners.SearchListener;
import model.Model;

import javax.swing.*;
import java.awt.*;

/**
 * A panel containing a combobox and a button.
 */
public class SearchBox extends JPanel {
    private JPanel searchPanel = new JPanel();
    private JComboBox searchComboBox = new JComboBox();
    private JButton searchButton;

    /**
     * Constructs new SearchBox.
     * @param sideMenu the side menu
     */
    public SearchBox(SideMenu sideMenu, Model m, CanvasView c) {
        sideMenu.makeAutoCompleteBox("Search", searchPanel, searchComboBox);

        searchButton = new JButton("Search");
        searchButton.setBackground(new Color(255, 255, 255));
        searchButton.addActionListener(new SearchListener(searchComboBox, m, c));

        this.setLayout(new BorderLayout());
        this.add(searchButton, BorderLayout.EAST);
        this.add(searchPanel, BorderLayout.CENTER);
    }
}

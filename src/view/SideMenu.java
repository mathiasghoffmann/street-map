package view;

import controller.listeners.FindRouteListener;
import controller.listeners.SearchTextFieldListener;
import model.Model;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

/**
 * A panel containing all the components for the side menu.
 */
public class SideMenu extends JPanel {
    private JPanel routeSelecter = new JPanel();
    private JPanel transportationSelecter = new JPanel();
    private JPanel startPanel = new JPanel();
	private JComboBox startComboBox = new JComboBox();
	private JPanel endPanel = new JPanel();
	private JComboBox endComboBox = new JComboBox();
	private JPanel searchRoutePanel = new JPanel();
	private JButton searchRoute = new JButton("Find Route");
	private ArrayList<JToggleButton> routeButtonList = new ArrayList<>();
	private JToggleButton fastestRoute = new JToggleButton("Fastest route");
	private JToggleButton shortestRoute = new JToggleButton("Shortest route");
	private ButtonGroup routeButtonGroup = new ButtonGroup();
    private JPanel directionListPanel = new JPanel();
	private JTextArea directionsList = new JTextArea();
    private JScrollPane scroll = new JScrollPane(directionsList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
    private ArrayList<JToggleButton> buttonList = new ArrayList<>();
    private JToggleButton walkButton = new JToggleButton("Walk");
    private JToggleButton bikeButton = new JToggleButton("Bike");
    private JToggleButton carButton = new JToggleButton("Car");
	private ButtonGroup vehicleButtonGroup = new ButtonGroup();
	private Model model;
	private CanvasView canvas;

    /**
     * Constructs new SideMenu.
     * @param m the model
     * @param c the canvas
     */
	public SideMenu(Model m, CanvasView c){
	    this.model = m;
	    this.canvas = c;

	    this.setBackground(Color.WHITE);
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createHorizontalStrut(10));
        this.add(Box.createVerticalStrut(10));

        carButton.setSelected(true);
        carButton.setBackground(new Color(255, 255, 255));
        carButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.DESELECTED) {
                    routeButtonGroup.clearSelection();
                    fastestRoute.setEnabled(false);
                    shortestRoute.setEnabled(false);
                }
                else if(e.getStateChange() == ItemEvent.SELECTED) {
                    fastestRoute.setEnabled(true);
                    shortestRoute.setEnabled(true);
                    fastestRoute.setSelected(true);
                }
            }
        });
        bikeButton.setBackground(new Color(255, 255, 255));
        bikeButton.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED) {

                }
            }
        });
        walkButton.setBackground(new Color(255, 255, 255));
        buttonList.add(carButton);
        buttonList.add(bikeButton);
        buttonList.add(walkButton);
        vehicleButtonGroup.add(carButton);
        vehicleButtonGroup.add(bikeButton);
        vehicleButtonGroup.add(walkButton);
        transportationSelecter.add(carButton);
        transportationSelecter.add(bikeButton);
        transportationSelecter.add(walkButton);
        transportationSelecter.setBackground(new Color(0,0,0, 0));
        this.add(transportationSelecter);
        this.add(Box.createVerticalStrut(10));
        startPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
        makeAutoCompleteBox("Start", startPanel, startComboBox);
        this.add(Box.createVerticalStrut(13));
        endPanel.setBorder(new EmptyBorder(0, 10, 0, 10));
        makeAutoCompleteBox("End", endPanel, endComboBox);
        this.add(Box.createVerticalStrut(13));

        // Sets the fastest route toggle button as selected, so it is selected, when the program opens
        fastestRoute.setSelected(true);

        // Makes the route buttons white;
        fastestRoute.setBackground(new Color(255, 255, 255));
        shortestRoute.setBackground(new Color(255, 255, 255));

        // Adds the route buttons to an arrayList to be used in the route planning
        routeButtonList.add(fastestRoute);
        routeButtonList.add(shortestRoute);

        // Adds the route buttons to a button group to make only sure only one can be selected at a time
        routeButtonGroup.add(fastestRoute);
        routeButtonGroup.add(shortestRoute);

        // Adds the route buttons to a panel and adds that panel to the side menu and creates some space below the
        // route buttons
        routeSelecter.add(fastestRoute);
        routeSelecter.add(shortestRoute);
        routeSelecter.setBackground(new Color(0,0,0, 0));
        this.add(routeSelecter);
        this.add(Box.createVerticalStrut(10));

        searchRoute.addActionListener(new FindRouteListener(buttonList, routeButtonList, startComboBox, endComboBox,
                model, canvas, directionsList));
        searchRoutePanel.setLayout(new BorderLayout());
        searchRoutePanel.add(searchRoute, BorderLayout.WEST);
        searchRoutePanel.setBackground(new Color(0, 0, 0, 0));
        searchRoutePanel.setBorder(new EmptyBorder(0, 10, 0, 10));
        searchRoute.setVisible(true);
        this.add(searchRoutePanel);
        this.add(Box.createVerticalStrut(13));

        directionListPanel.setLayout(new BorderLayout());
        directionListPanel.add(scroll);
        directionListPanel.setBackground(Color.WHITE);
        directionListPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        directionListPanel.setPreferredSize(new Dimension(0, 1000));
        directionListPanel.setVisible(true);
        directionsList.setBackground(new Color(240, 240, 240));
        directionsList.setEditable(false);
        directionsList.setVisible(true);
        this.add(directionListPanel);
        this.setVisible(true);
	}

    /**
     * Sets up the search combo-boxes.
     * @param s the default string for the combo-box
     * @param panel the panel holding the combo-box
     * @param comboBox the combo-box
     */
    public void makeAutoCompleteBox(String s, JPanel panel, JComboBox comboBox) {
	    panel.setLayout(new BorderLayout());
        panel.setPreferredSize(new Dimension(400, 50));
        panel.setBackground(Color.WHITE);
        this.add(panel);
        comboBox.setEditable(true);
        comboBox.getEditor().setItem(s);
        panel.add(comboBox, BorderLayout.CENTER);
        for(Component component : comboBox.getComponents()) {
            if(component instanceof JButton) {
                comboBox.remove(component);
            }
            else {
                comboBox.setLayout(new BorderLayout());
                comboBox.add(component, BorderLayout.CENTER);
            }
        }
        comboBox.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if(comboBox.getEditor().getItem().toString().equals(s)) {
                    comboBox.getEditor().setItem("");
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                if(comboBox.getEditor().getItem().toString().equals("")) comboBox.getEditor().setItem(s);
            }
        });
        comboBox.getEditor().getEditorComponent().addKeyListener(new SearchTextFieldListener(model, comboBox, canvas));
    }
}

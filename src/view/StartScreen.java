package view;

import controller.KeyboardController;
import controller.MouseController;
import model.Model;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A screen, where users can choose either to open the default file (program arguments or ressource) or load their own.
 */
public class StartScreen {
    // http://javarevisited.blogspot.dk/2011/09/invokeandwait-invokelater-swing-example.html KODE TIL INVOKE AND WAIT
    private LoadingPopUp loadingPopUp;

    /**
     * Constructs a new StartScreen.
     * @param args the program arguments if any
     */
    public StartScreen(String[] args) {
        Runnable loading = () -> loadingPopUp = new LoadingPopUp();

        // Setting up the frame for the start screen here;
        JFrame frame = new JFrame("Førsteårsprojekt");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().setBackground(new Color(255,255,255));
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.setPreferredSize(new Dimension(350, 200));

        frame.setPreferredSize(new Dimension(350, 200));

        // Setting up the open button from here
        JButton open = new JButton("Open default");
        open.setAlignmentX(JButton.CENTER_ALIGNMENT);
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (args.length == 0) {
                    frame.dispose();
                    Thread thread = new Thread() {
                        public void run() {
                            try {
                                SwingUtilities.invokeAndWait(loading);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                            Model model = new Model();
                            model.jarLoad("default-denmark.bin");
                            loadingPopUp.getFrame().dispose();
                            setUpMainWindow(model);
                        }
                    };
                    thread.start();
                } else {
                    frame.dispose();
                    Thread thread = new Thread() {
                        public void run() {
                            try {
                                SwingUtilities.invokeAndWait(loading);
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                            Model model = new Model();
                            model.load(args[0]);
                            loadingPopUp.getFrame().dispose();
                            setUpMainWindow(model);
                        }
                    };
                    thread.start();
                }
            }
        });

        // Setting up the load button from here
        JButton load = new JButton("Load your own file");
        load.setAlignmentX(JButton.CENTER_ALIGNMENT);
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter("bin files, osm files and zip directories", "bin", "osm", "zip");
                fileChooser.setFileFilter(filter);
                int returnVal = fileChooser.showOpenDialog(frame);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    frame.dispose();
                    Thread thread = new Thread() {
                        public void run() {
                            try {
                                SwingUtilities.invokeAndWait(loading);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Model model = new Model();
                            model.load(fileChooser.getSelectedFile().getPath());
                            loadingPopUp.getFrame().dispose();
                            setUpMainWindow(model);
                        }
                    };
                    thread.start();
                }
            }
        });

        // Adding buttons to frame and doing some other small changes to frame
        frame.add(Box.createVerticalStrut(50));
        frame.add(open);
        frame.add(Box.createVerticalStrut(13));
        frame.add(load);
        frame.add(Box.createVerticalStrut(50));
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setResizable(false);
    }

    /**
     * Sets up the whole user interface, when the model is finished loading.
     * @param model the current model
     */
    private void setUpMainWindow(Model model) {
        Toolbar tb = new Toolbar();
        CanvasView cv = new CanvasView(model, tb);
        SideMenu sm = new SideMenu(model, cv);
        SearchBox sb = new SearchBox(sm, model, cv);
        MainWindowView v = new MainWindowView(sb, cv, sm, tb, model);
        new KeyboardController(v, cv, model);
        new MouseController(cv, model, tb);
        cv.repaint();
    }
}

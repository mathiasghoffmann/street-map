package view;

import javax.swing.*;
import java.awt.*;

/**
 * Panel that shows information about the nearest road and the zoom level.
 */
public class Toolbar extends JPanel {
    private JTextField nearestRoad;
    private JTextField zoomNumber;

    /**
     * Constructs a new Toolbar.
     */
    public Toolbar() {
        zoomNumber = new JTextField("");
        nearestRoad = new JTextField("");

        this.setLayout(new BorderLayout());

        this.add(nearestRoad, BorderLayout.CENTER);
        this.add(zoomNumber, BorderLayout.EAST);

        zoomNumber.setPreferredSize((new Dimension(200, 37)));
        zoomNumber.setEditable(false);

        nearestRoad.setEditable(false);
    }

    /**
     * Sets the text of this ToolBar's nearestRoad textfield to the specified value.
     * @param text the text to be expressed in the nearestRoad textfield
     */
    public void setNearestRoad(String text) {
        nearestRoad.setText(text);
    }

    /**
     * Sets the text of this ToolBar's zoomNumber textfield to the specified value.
     * @param z the number to be expressed in the zoomNumber textfield
     */
    public void setZoomNumber(double z) {
        int zoombarnumber = (int) z;
        if (z < 5000) {
            zoomNumber.setText("" + zoombarnumber + " Meters");
        } else {
            zoomNumber.setText("" + zoombarnumber / 1000 + " Km");
        }
    }
}
